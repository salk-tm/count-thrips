# count-thrips

## Using the Docker Images

This section provides instructions on how to pull and run the Docker images for the `detect-qrs-app` and `count-thrips-app` applications. These images are designed to process data through a two-step pipeline, utilizing input, intermediate, and output directories.

### Prerequisites

- Docker installed on your machine. If you do not have Docker installed, please follow the [official Docker installation guide](https://docs.docker.com/get-docker/).
- Access to a terminal or command prompt.

### Pulling the Docker Images

To pull the latest versions of the Docker images from the registry using the following commands:

```sh
docker pull registry.gitlab.com/salk-tm/count-thrips/detect-qrs-app:latest
docker pull registry.gitlab.com/salk-tm/count-thrips/count-thrips-app:latest
```

### Running the Docker Containers

## Using the `run_pipeline.sh` Script

The `run_pipeline.sh` script runs the `detect-qrs-app` and `count-thrips-app` containers.

### Script Overview

The `run_pipeline.sh` script requires three arguments:

1. The path to your input directory.
2. The path to your intermediate directory (used for storing data between steps).
3. The path to your output directory.

### Running the Script

1. Open a terminal or command prompt.
2. Navigate to the directory where `run_pipeline.sh` is located.
3. Run the script by providing the paths to your input, intermediate, and output directories. For example:

```bash
./run_pipeline.sh "/path/to/input" "/path/to/intermediate" "/path/to/output"
```
Replace `"/path/to/input"`, `"/path/to/intermediate"`, and `"/path/to/output"` with the actual paths to your directories.

- Your data will be output in the intermediate and output directories. You should **copy it to a secure place** after each run to avoid it being overwritten in the future.
- You can clean your input, intermediate and output directories after you save your data in a secure place.

### Using the `run_pipeline.sh` Script on the PBIOB-GH-04 Computer "Lil' Beastie"

1. Make your `design.xlsx` file with columns "qr_code",	"date_placed",	"date_collected", "table", "bay" that are filled with qr codes.
2. Add `design.xlsx` and images of thrips to the input directory: `/mnt/d/count_thrips_input`.
3. Open Ubuntu using Windows search bar.
4. Change directories to location of run script: `cd /mnt/d/repos/count-thrips`.
5. Run the script with input, intermediate and output directories defined: `./run_pipeline.sh /mnt/d/count_thrips_input /mnt/d/count_thrips_intermediate /mnt/d/count_thrips_output`
6. Copy intermediate and output data to a secure location.
7. Clean input, intermediate and output directories once your data is saved elsewhere. 

### Fixing `command not found` and `syntax error` in `run_pipeline.sh`

If you encounter the following errors:

```
./run_pipeline.sh: line 3: $'\r': command not found
./run_pipeline.sh: line 56: syntax error: unexpected end of file
```

It’s likely due to Windows-style line endings (`\r\n`). To fix this on WSL (Ubuntu):

1. **Install `dos2unix` (if not already installed):**
   ```bash
   sudo apt-get install dos2unix
   ```

2. **Convert the script to Unix-style line endings:**
   ```bash
   dos2unix run_pipeline.sh
   ```

3. **Re-run the script:**
   ```bash
   ./run_pipeline.sh /mnt/d/count_thrips_input /mnt/d/count_thrips_intermediate /mnt/d/count_thrips_output
   ```

This should resolve the errors.

### Run the Docker Images Interactively on Lil'Beastie

### detect-qrs-app

```
docker run --gpus all -v /mnt/d/count_thrips_input:/workspace/input -v /mnt/d/count_thrips_intermediate:/workspace/output -it registry.gitlab.com/salk-tm/count-thrips/detect-qrs-app:latest bash
python /workspace/detect_qrs/main.py /workspace/input /workspace/output
```

or on Windows

```
docker run --gpus all -v D:/count_thrips_input:/workspace/input -v D:/count_thrips_intermediate:/workspace/output -it registry.gitlab.com/salk-tm/count-thrips/detect-qrs-app:latest bash   
python /workspace/detect_qrs/main.py /workspace/input /workspace/output
```

### count-thrips-app
```
docker run --gpus all -v /mnt/d/count_thrips_intermediate:/workspace/input -v /mnt/d/count_thrips_output:/workspace/output -it registry.gitlab.com/salk-tm/count-thrips/count-thrips-app:latest bash
python /workspace/count_thrips/main.py /workspace/input /workspace/output
```

or on Windows

```
docker run --gpus all -v D:/count_thrips_intermediate:/workspace/input -v D:/count_thrips_output:/workspace/output -it registry.gitlab.com/salk-tm/count-thrips/count-thrips-app:latest bash   
python /workspace/count_thrips/main.py /workspace/input /workspace/output
```

### Explanation of the `run_pipeline.sh` script

#### Step 1: Run `detect-qrs-app`

```bash
docker run --gpus all -v /path/to/your/input:/workspace/input -v /path/to/your/intermediate:/workspace/output registry.gitlab.com/salk-tm/count-thrips/detect-qrs-app:latest python /workspace/detect_qrs/main.py /workspace/input /workspace/output
```

#### Step 2: Run `count-thrips-app`

```bash
docker run --gpus all -v /path/to/your/intermediate:/workspace/input -v /path/to/your/output:/workspace/output registry.gitlab.com/salk-tm/count-thrips/count-thrips-app:latest python /workspace/count_thrips/main.py /workspace/input /workspace/output
```

## Description of the repository
There are two parts.

# Part 1: Detect QR Codes
Package that has qreader environment.

The `main` function is located at `"detect_qrs/detect_qrs/main.py"`

## Arguments 
- **`input_dir`**: The directory containing the necessary input files.
  - **`design.xlsx`**: Excel file with columns "qr_code",	"date_placed",	"date_collected", "table". Only qr codes included in this file will have results.
  - **Images**: Images uploaded from Box app with filename format "Image 1-16-24, 10.58.20 AM.jpg".

- **`output_dir`**: The directory where the output files will be made. This folder is the input to next part.
  -**`design_qr_codes.xlsx`**: Excel file with columns "qr_code",	"date_placed",	"date_collected",	"table",	"image_path",	"date_imaged",	"time_imaged". The new columns are filled with the information from the found images and merged by detected `qr_code`. 
  - Images with sanitized filenames.
  - Logging info from the detect-qrs-app.


## Tests
- Tests are located at `"detect_qrs/tests/test_detect_qrs.py"`
- Test fixtures are located at `"detect_qrs/tests/fixtures/data.py"`.
- Find test data for expected inputs at `"detect_qrs\tests\data"`.

# Part 2: Count Thrips

The `main` function is located at `"count_thrips/count_thrips/main.py"`

## Arguments

- **`input_dir`**: The directory containing the necessary input files.
  - **`params.json`**: Contains all the parameters needed to run the count-thrips pipeline.  
    - `overwrite`: If False, load existing predictions instead of re-saving. Can be `False` or `True`. Default value is `False`.
    - `pred_name` # Used to save or load predictions for each image. Default value is `thrip`.
    - Example:
      ```json
      {
          "overwrite": false,
          "pred_name": "thrip",
        }
      ```
  - Images with sanitized filenames from part 1. Thrips are detected on these images.
  - Logging info from the detect-qrs-app (not necessary for this part).


- **`output_dir`**: The directory where the output files from the data cleanup pipeline will be saved.
  - **Output Files**: Running the pipeline generates several output files saved in the specified `output_dir`:
    - **`thrip_counts.xlsx`**: Thrip counts merged with experimental design info.
    - Prediction files for each image with a sanitized filename. For example: "Image_1-16-24,_10.58.20_AM.thrip.predictions.slp".

## Tests
- Tests are located at `"count_thrips/tests/test_count_thrips.py"`
- Test fixtures are located at `"count_thrips/tests/fixtures/data.py"`.


## Notes
- Ensure that the `input_dir` contains the correctly formatted `design.xlsx` and correctly named images with thrips before running the pipeline.
- Review and adjust the parameters in `params.json` in the intermediate folder (input to step 2) as needed to fit specific requirements, otherwise
defaults will be used.

## References: 
- https://github.com/Eric-Canas/QReader
- https://pypi.org/project/qreader/