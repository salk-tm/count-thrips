import os
import shutil
import sys
import logging
import pandas as pd
import openpyxl
import sys

from detect_qrs.detect_qrs_util import detect_qr_codes
from detect_qrs.image_util import find_images, parse_datetime_from_filename, rename_and_copy_images_to_output_dir
from detect_qrs.log_util import setup_logging
from pathlib import Path


def main(input_dir: str, output_dir: str) -> dict:
    """Main function to
    1) sanitize filenames in the input directory and copy them to the output directory
    2) find images in the output directory
    3) detect QR codes in each image
    4) merge the detected QR codes with design.xlsx
    5) save the merged results to an Excel file in the output directory

    Parameters:
    - input_dir (str): Path to the folder containing the images and "design.xlsx".
    - output_dir (str): Directory where the log file, renamed images, and 
                        "design_qr_codes.xlsx" will be saved.

    Returns:
    - dict: A dictionary where keys are the sanitized image filenames and values are
            the detected QR codes data for each image.
    """
    input_dir = Path(input_dir)
    output_dir = Path(output_dir)

    # Set up logging
    setup_logging(output_dir)
    logging.info("Starting qr code detection.")

    # Load design.xlsx as a DataFrame
    design_file = input_dir / "design.xlsx"
    try:
        design_df = pd.read_excel(design_file, engine="openpyxl")
    except Exception as e:
        logging.error(f"Error reading design file: {e}")
        return

    # Rename files in the input directory with sanitized filenames
    rename_and_copy_images_to_output_dir(input_dir, output_dir)
    # Find renamed images in the output folder
    image_files = find_images(output_dir)
    if not image_files:
        logging.error(f"No images found in {output_dir}.")
        return

    # Detect QR codes in each image
    qr_codes_per_image = {}
    for image_file in image_files:
        qr_codes_data = detect_qr_codes(image_file)
        # If `image_file` is not in `qr_codes_data`, add it with an empty list
        qr_codes_per_image[image_file] = qr_codes_data.get(image_file, [])

    # Create a DataFrame to store the QR code results
    data = []
    for img_path, codes in qr_codes_per_image.items():
        img_name = img_path.name
        date_imaged, time_imaged = parse_datetime_from_filename(img_path)
        logging.debug(
            f"Image: {img_name}, QR codes: {codes}, Date: {date_imaged}, Time: {time_imaged}")
        data.append((img_name, str(img_path), ", ".join(
            filter(None, codes)), date_imaged, time_imaged))
        logging.debug(f"Data: {data}")

    results_df = pd.DataFrame(
        data, columns=["image_name", "image_path",
                       "qr_code", "date_imaged", "time_imaged"]
    )

    # Verify 'qr_code' column exists in both DataFrames
    if 'qr_code' not in design_df.columns:
        raise ValueError(
            "The 'qr_code' column is missing from the design DataFrame.")
    if 'qr_code' not in results_df.columns:
        raise ValueError(
            "The 'qr_code' column is missing from the results DataFrame.")

    # Verify the 'qr_code' column format in both DataFrames
    if not pd.api.types.is_string_dtype(design_df['qr_code']):
        raise TypeError(
            "The 'qr_code' column in the design DataFrame is not of string type.")
    if not pd.api.types.is_string_dtype(results_df['qr_code']):
        raise TypeError(
            "The 'qr_code' column in the results DataFrame is not of string type.")

    # Merge with design_df on the 'qr_code' column
    # All qr codes from design are included, even if they are not detected
    merged_df = design_df.merge(results_df, on="qr_code", how="left")
    logging.debug(f"Merged DataFrame: {merged_df}")

    # Save the merged results to an Excel file in the output directory
    merged_output_file = output_dir / "design_qr_codes.xlsx"
    merged_df.to_excel(merged_output_file, index=False)
    logging.info(f"Merged results saved to {merged_output_file}")

    return qr_codes_per_image


if __name__ == "__main__":
    input_dir = sys.argv[1]
    output_dir = sys.argv[2]
    main(input_dir, output_dir)
