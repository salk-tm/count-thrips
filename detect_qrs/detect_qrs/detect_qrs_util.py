from pathlib import Path
import cv2
import pandas as pd
import logging

from qreader import QReader
from typing import Dict, List


def detect_qr_codes(image_file: Path) -> Dict[Path, List[str]]:
    """Detects QR codes in a single image.

    Parameters:
    - image_file (Path): Path to the image file.

    Returns:
    - dict: A dictionary where the key is the image file path and the value is
            the detected QR codes data for the image.
    """
    logging.info(f"Processing image: {image_file.name}")

    # Get the image that contains the QR code
    image = cv2.cvtColor(cv2.imread(str(image_file)), cv2.COLOR_BGR2RGB)

    # Create a QReader instance
    qreader = QReader()

    # Use the detect_and_decode function to get the decoded QR data
    qr_codes_data = qreader.detect_and_decode(image=image)

    if qr_codes_data:
        logging.info(
            f"Detected {len(qr_codes_data)} QR code(s) in {image_file.name}")
        logging.info(f"QR codes found: {qr_codes_data}")
        return {image_file: qr_codes_data}
    else:
        logging.warning(f"No QR codes detected in {image_file.name}")
        return {image_file: ['no qr codes detected']}
