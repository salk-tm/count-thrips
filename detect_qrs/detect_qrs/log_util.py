import os
import sys
import logging


class StreamToLogger:
    """Fake file-like stream object that redirects writes to a logger instance.

    Attributes:
        logger (logging.Logger): Logger object to which the stream is redirected.
        log_level (int): Logging level at which the messages are logged.
        linebuf (str): Buffer to hold the stream content before it's flushed to the logger.

    Methods:
        write(buf): Writes the buffer 'buf' to the logger at the specified log level.
        flush(): Placeholder method to mimic file-like stream. Actual flushing is handled by the logger.
    """

    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level
        self.linebuf = ""

    def write(self, buf):
        """Writes the buffer 'buf' to the logger at the specified log level.

        Args:
            buf (str): String to be written to the logger.
        """
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

    def flush(self):
        """Flushes the stream.

        As the actual flushing is handled by the logger, this method is a placeholder
        to maintain the interface of a file-like stream.
        """
        pass


def setup_logging(output_dir):
    """Sets up the logging configuration to log all messages to a file and redirect
    stdout and stderr to the logger.

    Args:
        output_dir (str): Directory where the log file will be saved.

    The function configures the root logger to write all messages to a file
    in the specified output directory. 
    It also replaces sys.stdout and sys.stderr with instances of StreamToLogger 
    to redirect all print statements and error messages to the logger.
    """
    log_file = os.path.join(output_dir, "detect_qrs.log")

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s: %(levelname)s: %(message)s",
        filename=log_file,
        filemode="a",
    )

    # Redirect stdout and stderr to the logger
    stdout_logger = logging.getLogger("STDOUT")
    sl = StreamToLogger(stdout_logger, logging.INFO)
    sys.stdout = sl

    stderr_logger = logging.getLogger("STDERR")
    sl = StreamToLogger(stderr_logger, logging.ERROR)
    sys.stderr = sl
