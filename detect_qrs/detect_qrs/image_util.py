import datetime
from pathlib import Path
import shutil
import logging
from typing import List


def find_images(folder_path: str) -> List[Path]:
    """Finds all the images in the specified folder.

    Parameters:
    - folder_path (str): Path to the folder containing the images.

    Returns:
    - list: A list of Paths to the image files.
    """
    folder = Path(folder_path)
    if not folder.is_dir():
        logging.info(f"Error: '{folder_path}' is not a valid directory.")
        return []

    # Patterns for image files
    image_patterns = ["*.jpg", "*.png"]
    image_files = []

    # Iterate over each pattern and extend the list of image files
    for pattern in image_patterns:
        image_files.extend(folder.glob(pattern))

    return image_files


def sanitize_filename(filename: str) -> str:
    """Sanitizes the filename by replacing non-standard whitespace and potentially
    problematic characters with standard or safe alternatives.

    Parameters:
    - filename (str): The original filename that may contain non-standard characters.

    Returns:
    - str: The sanitized filename with non-standard characters replaced.
    """
    # Replace non-breaking spaces with regular spaces
    sanitized = filename.replace('\u202F', '_').replace(
        '\u00A0', '_').replace(' ', '_')

    return sanitized


def rename_and_copy_images_to_output_dir(input_dir: str, output_dir: str) -> None:
    """Sanitizes the filenames of the images in the input directory and copies them to 
    the output directory. Uses the `sanitize_filename` function.

    Parameters:
    - input_dir (str): The path to the directory containing the original files.
    - output_dir (str): The path to the directory where sanitized copies of the files will be stored.

    Returns:
    - None: Files are copied and renamed in the output directory, with no return value.
    """
    input_dir_path = Path(input_dir)
    output_dir_path = Path(output_dir)
    # Ensure the output directory exists
    output_dir_path.mkdir(parents=True, exist_ok=True)

    # Iterate over each file in the input directory
    for path in input_dir_path.iterdir():
        if path.is_file() and path.suffix.lower() in ['.jpg', '.png']:
            # Sanitize filename
            new_name = sanitize_filename(path.name)
            new_path = output_dir_path / new_name
            # Copy and rename the file
            shutil.copy2(path, new_path)
            logging.info(f"Copied and renamed '{path.name}' to '{new_path}'")


def parse_datetime_from_filename(filename: Path) -> tuple[str, str]:
    """Extracts date and time from a filename with a specific format.

    Assumes filename format: "Image_MM-DD-YY_HH.MM.SS_AM/PM.jpg", extracting
    and converting the date and time components into datetime objects.

    Parameters:
    - filename (Path): Filename to parse, expected in the specified format.

    Returns:
    - tuple(str, str): Extracted date and time. Returns (None, None) on parsing failure.
    """
    try:
        # Convert Path object to string and extract components
        # Use .stem to exclude the file extension
        filename_str = str(filename.stem)

        # Splitting based on underscores and extracting date and time parts
        parts = filename_str.split("_")
        date_str = parts[1].replace(",", "")
        # Include AM/PM part for correct time parsing
        time_str = parts[2] + "_" + parts[3]
        return date_str, time_str
    except Exception as e:
        logging.info(
            f"Error parsing date and time from filename {filename}: {e}")
        return None, None
