import datetime
import pytest
import logging
import pandas as pd
import tempfile

from pathlib import Path

from detect_qrs.main import main
from detect_qrs.detect_qrs_util import detect_qr_codes
from tests.fixtures.data import data_folder, output_folder
from detect_qrs.image_util import find_images, parse_datetime_from_filename

# setup logging
logging.basicConfig(level=logging.DEBUG)


@pytest.mark.parametrize(
    "filename, expected",
    [
        (
            Path("Image_1-16-24,_10.58.20_AM.jpg"),
            ("1-16-24", "10.58.20_AM"),
        ),
        (
            Path("Image_1-16-24,_10.59.07_AM.jpg"),
            ("1-16-24", "10.59.07_AM"),
        ),
    ],
)
def test_parse_datetime_from_filename_dummy_data(filename, expected):
    assert parse_datetime_from_filename(filename) == expected


# Test with invalid data
@pytest.mark.parametrize(
    "filename",
    [
        Path("InvalidFormat.jpg"),
        Path("Image_,_.jpg"),
    ],
)
def test_parse_datetime_from_filename_invalid_data(filename):
    assert parse_datetime_from_filename(filename) == (None, None)


# Test main function
def test_main(data_folder):
    # Create a temporary directory for output
    with tempfile.TemporaryDirectory() as temp_dir:
        output_folder = Path(temp_dir)

        # Call the main function
        main(data_folder, output_folder)

        # Check the output file
        output_file = output_folder / "design_qr_codes.xlsx"
        assert output_file.is_file()

        # Check the content of the output file
        df = pd.read_excel(output_file, engine="openpyxl")

        # Print the DataFrame for debugging
        print(df)

        # Check that the correct number of rows are present
        assert len(df) == 4

        # Check that the correct QR codes are present
        assert sorted(df["qr_code"].dropna().tolist()) == sorted([
            "3.12.2",
            "3.12.1",
            "2.2.2",
            "3.6.1",
        ])
