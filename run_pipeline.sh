# Description: Run the entire pipeline
# Example: ./run_pipeline.sh <input_dir> <intermediate_dir> <output_dir>

set -e # Exit immediately if a command exits with a non-zero status
set -x # Print commands and their arguments as they are executed


# Check if correct number of arguments is provided
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <input_dir> <intermediate_dir> <output_dir>"
    exit 1
fi

INPUT_DIR=$1
INTERMEDIATE_DIR=$2
OUTPUT_DIR=$3

# Names or paths of Docker images
IMAGE_DETECT_QRS=registry.gitlab.com/salk-tm/count-thrips/detect-qrs-app:latest
IMAGE_COUNT_THRIPS=registry.gitlab.com/salk-tm/count-thrips/count-thrips-app:latest

# Make sure local images are up-to-date
echo "Pulling Docker images..."
docker pull $IMAGE_DETECT_QRS
docker pull $IMAGE_COUNT_THRIPS
echo "Docker images are up-to-date."


# Run the first Docker container with GPU support
echo "Running detect-qrs-app Docker container..."
docker run --gpus all -v "$INPUT_DIR":/workspace/input -v "$INTERMEDIATE_DIR":/workspace/output $IMAGE_DETECT_QRS python /workspace/detect_qrs/main.py /workspace/input /workspace/output

if [ $? -ne 0 ]; then
    echo "Error: Failed to run detect-qrs-app Docker container."
    exit 1
fi

echo "detect-qrs-app ran successfully."

# Run the second Docker container with GPU support
echo "Running count-thrips-app Docker container..."
docker run --gpus all -v "$INTERMEDIATE_DIR":/workspace/input -v "$OUTPUT_DIR":/workspace/output $IMAGE_COUNT_THRIPS python /workspace/count_thrips/main.py /workspace/input /workspace/output

if [ $? -ne 0 ]; then
    echo "Error: Failed to run count-thrips-app Docker container."
    exit 1
fi

echo "count-thrips-app ran successfully. Check $OUTPUT_DIR for the output files."

# Output image information for both images
echo "Image information for $IMAGE_DETECT_QRS:" > "$OUTPUT_DIR/image_info.txt"
docker inspect $IMAGE_DETECT_QRS >> "$OUTPUT_DIR/image_info.txt"

echo "" >> "$OUTPUT_DIR/image_info.txt"  # Add a newline for separation
echo "Image information for $IMAGE_COUNT_THRIPS:" >> "$OUTPUT_DIR/image_info.txt"
docker inspect $IMAGE_COUNT_THRIPS >> "$OUTPUT_DIR/image_info.txt"

# Append GPU information to the image_info.txt file
echo "" >> "$OUTPUT_DIR/image_info.txt"
echo "GPU information:" >> "$OUTPUT_DIR/image_info.txt"
nvidia-smi >> "$OUTPUT_DIR/image_info.txt"

echo "Pipeline completed successfully."

