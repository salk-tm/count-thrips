import json
import sleap
import sys
import openpyxl
import logging
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
import os

from pathlib import Path
from rich.progress import track
from glob import glob
from count_thrips.get_predictions import get_predictions, find_centroid_model, find_centered_instance_model, get_thrip_count
from count_thrips.find_images import find_images
from count_thrips.log_util import setup_logging, StreamToLogger


def main(input_dir:str, output_dir:str):
    """Main function to
    1) import "design_qr_codes.xlsx" from "input_dir"
    2) detect thrips in the images in "input_dir"
    3) count the thrips in the images
    4) merge the thrips counts with the design data
    5) save the merged data to "output_dir/thrips_counts.xlsx"

    Parameters:
    - input_dir (str): Path to the folder containing the images.
    - output_dir (str): Directory where the processed images will be saved.

    Returns:
    - dict: A dictionary where keys are the image filenames and values are
            the number of detected thrips.
    """
    input_dir = Path(input_dir)
    output_dir = Path(output_dir)

    # Model directory is a subdirectory of the package
    model_dir = Path("count_thrips") / "models"

    # Set up logging
    setup_logging(output_dir)
    logging.info("Starting thrip counting.")

    # Load design_qr_codes.xlsx as a DataFrame
    design_file = input_dir / "design_qr_codes.xlsx"
    try:
        design_df = pd.read_excel(design_file, engine="openpyxl")
    except Exception as e:
        logging.error(f"Error reading design file: {e}")
        return

    logging.info(f"sleap versions: {sleap.versions()}")

    # Find images in the input directory
    images = find_images(input_dir)

    # Get models
    logging.info("Loading models.")
    try:
        centroid_path = find_centroid_model(model_dir).as_posix()
        centered_path = find_centered_instance_model(model_dir).as_posix()
        predictor = sleap.load_model([centroid_path, centered_path], progress_reporting="none")
    except FileNotFoundError as e:
        logging.error(f"Error finding model: {e}")
        return

    # Get parameters
    logging.info("Getting parameters.")
    params = get_params(input_dir)
    overwrite = get_overwrite(params)
    pred_name = get_pred_name(params)

    # Get predictions
    logging.info("Getting predictions.")
    thrips_counts = {}
    for filename in track(images):
        image_name = filename.name  # Extract just the image name
        # Get the predictions for this filename
        pred_path = get_predictions(output_dir, filename, pred_name, predictor, overwrite)
        
        # Initialize a dictionary for this filename and save the prediction path and thrip count
        thrips_counts[image_name] = {
            'pred_path': pred_path,
            'thrip_count': get_thrip_count(pred_path)
        }

    # After collecting all data, prepare for merging
    logging.info("Merging thrips counts with design data.")
    thrips_df = pd.DataFrame.from_dict(thrips_counts, orient="index")
    thrips_df.reset_index(inplace=True)
    thrips_df.rename(columns={'index': 'image_name'}, inplace=True)  # Use 'image_name' for clarity

    # Adjust the merge operation to use 'image_name'
    merged_df = pd.merge(design_df, thrips_df, left_on="image_name", right_on="image_name", how="left")
    # Save the merged data to an excel file
    merged_file = output_dir / "thrips_counts.xlsx"
    merged_df.to_excel(merged_file, index=False)
    logging.info(f"Thrips counts saved to {merged_file}.")

    return thrips_counts



def get_params(input_dir: Path) -> dict:
    """Read the parameters from the input directory from the `params.json` file.

    Args:
    - input_dir (Path): Path to the input directory. This directory should contain
                        a `params.json` file.

    Returns:
    - dict: A dictionary containing the parameters.
    """
    params = {}
    params_file = input_dir / "params.json"
    if params_file.exists():
        with open(params_file, "r") as f:
            params = json.load(f)
        return params
    else:
        logging.info(f"params.json not found in {input_dir}. Defaults used.")
        return params
    

def get_overwrite(params: dict) -> bool:
    """Get the `overwrite` parameter from the parameters.

    Args:
    - params (dict): A dictionary containing the parameters.

    Returns:
    - bool: The value of the `overwrite` parameter. If the parameter is not found,
            the default value is `False`.
    """
    return params.get("overwrite", False)


def get_pred_name(params: dict) -> str:
    """Get the `pred_name` parameter from the parameters.

    Args:
    - params (dict): A dictionary containing the parameters.

    Returns:
    - str: The value of the `pred_name` parameter. If the parameter is not found,
            the default value is `thrip`.
    """
    return params.get("pred_name", "thrip")

if __name__ == "__main__":
    input_dir = sys.argv[1]
    output_dir = sys.argv[2]
    main(input_dir, output_dir)