import datetime
from pathlib import Path
from typing import List


def find_images(folder_path: str) -> List[Path]:
    """Finds all the images in the specified folder.

    Parameters:
    - folder_path (str): Path to the folder containing the images.

    Returns:
    - list: A list of Paths to the image files.
    """
    folder = Path(folder_path)
    if not folder.is_dir():
        print(f"Error: '{folder_path}' is not a valid directory.")
        return []

    # Patterns for image files
    image_patterns = ["*.jpg", "*.png"]
    image_files = []

    # Iterate over each pattern and extend the list of image files
    for pattern in image_patterns:
        image_files.extend(folder.glob(pattern))

    return image_files
