import sleap
from rich.progress import track
from pathlib import Path

def find_centroid_model(model_dir: Path) -> Path:
    """Find the centroid model in the given directory.

    Args:
    - model_dir (Path): Directory to search.

    Returns:
    - Path to the centroid model zip file.

    Raises:
    - FileNotFoundError if no matching file is found.
    """
    for model_path in model_dir.glob("*.zip"):
        if "centroid" in model_path.name:
            return model_path
    raise FileNotFoundError("No centroid model found in the given directory.")


def find_centered_instance_model(model_dir: Path) -> Path:
    """Find the centered instance model in the given directory.

    Args:
    - model_dir (Path): Directory to search.

    Returns:
    - Path to the centered instance model zip file.

    Raises:
    - FileNotFoundError if no matching file is found.
    """
    for model_path in model_dir.glob("*.zip"):
        if "centered_instance" in model_path.name:
            return model_path
    raise FileNotFoundError("No centered instance model found in the given directory.")


def get_predictions(output_dir: Path, filename: Path, pred_name: str, predictor: sleap.nn.inference.Predictor, overwrite: bool) -> Path:
    """Get predictions for the given image file.
    
    Args:
    - output_dir: Directory to save the predictions to.
    - filename: Path to the image file.
    - pred_name: Name to tag prediction filenames with.
    - predictor: The predictor to use for making predictions.
    - overwrite: Whether to overwrite existing predictions.
    """
    # Get the filename without the extension
    base_filename = str(filename.stem)
    
    # Construct the prediction filename
    pred_filename = f"{base_filename}.{pred_name}.predictions.slp"
    
    # Combine with the output directory to get the full path
    pred_path = (Path(output_dir) / pred_filename).as_posix()

    if Path(pred_path).exists() and not overwrite:
        print(f"Predictions already exist for {filename}. Skipping.")
        return pred_path
    else:
        image = sleap.load_video(filename.as_posix())
        predictions = predictor.predict(image)
        predictions.save(pred_path)
        print(f"Predictions saved to {pred_path}.")
        return pred_path
    

def get_thrip_count(pred_path: str) -> int:
    """Count the number of thrips in the given prediction file.
    
    Args:
    - pred_path (str): Path to the prediction file.
    
    Returns:
    - The number of thrips in the prediction file.
    """
    predictions = sleap.load_file(pred_path)
    return len(predictions.labeled_frames[0].instances)
