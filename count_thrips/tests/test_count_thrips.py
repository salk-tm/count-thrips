import pytest
from pathlib import Path
import pandas as pd
import openpyxl

from count_thrips.main import main
from count_thrips.get_predictions import get_predictions, find_centroid_model, find_centered_instance_model, get_thrip_count
from count_thrips.find_images import find_images
from count_thrips.count_thrips.log_util import setup_logging, StreamToLogger
from tests.fixtures.data import data_folder, output_folder



def test_main(data_folder, output_folder):
    # Call the main function
    thrip_count_dict = main(data_folder, output_folder)

    # Check the output file
    output_file = output_folder / "thrips_counts.xlsx"
    assert output_file.is_file()

    # Check the dictionary
    assert isinstance(thrip_count_dict, dict)
    assert len(thrip_count_dict) == 2
    assert thrip_count_dict["Image_1-16-24,_10.58.20_AM.jpg"]["thrip_count"] == 15

