import pytest
from pathlib import Path


@pytest.fixture
def data_folder():
    """Path to a folder with `design_qr_codes.xlsx` and images."""
    return Path("tests") / "data"


@pytest.fixture
def output_folder():
    """Path to a folder where the output of `main` should be saved."""
    return Path("tests") / "output_dir"
